import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import root from './root';

const logger = store => next => action => {
  console.log("Dispatching Action:\n", JSON.stringify(action, null, 4), '\n');
  const result = next(action);
  console.log('Next State:\n', JSON.stringify(store.getState(), null, 4));
  return result;
};

export default applyMiddleware(thunk, logger)(createStore)(root);